package com.example.myapplication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.morgoo.droidplugin.pm.PluginManager;
import com.morgoo.helper.compat.PackageManagerCompat;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private TextView tvTest;
    private TextView tvResult;
    private File[] plugins;
    private Button btnInstall;
    private Button btnTest;
    private int a;

    private static final int REQUEST_CODE_READ_EXTERNAL_STORAGE_PERMISSIONS = 1;
    private static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //动态请求权限
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                        new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                        REQUEST_CODE_READ_EXTERNAL_STORAGE_PERMISSIONS);
                requestPermissions(
                        new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE },
                        REQUEST_CODE_WRITE_EXTERNAL_STORAGE);
            }
        }
        setContentView(R.layout.activity_main);
        tvTest = (TextView) findViewById(R.id.tv_test);
        tvResult = (TextView) findViewById(R.id.tv_result);
        btnTest = (Button) findViewById(R.id.btn_test);
        btnInstall = (Button) findViewById(R.id.btn_install);


        //安装apk
        btnInstall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //获取插件
                File file = new File(Environment.getExternalStorageDirectory(), "/plugin");
                plugins = file.listFiles();
                //没有插件
                if (plugins == null || plugins.length == 0) {
                    Log.d("sss", "插件安装失败==没有找到插件");
                    return;
                }
                //1.先卸载apk
                try {
                    PluginManager.getInstance().deletePackage("com.pugin", 0);
                } catch (RemoteException e) {
                    Log.d("sss", "插件卸载失败=="+e.getMessage().toString());
                    e.printStackTrace();
                }

                for (File apk:plugins ) {
                    if (!apk.getAbsolutePath().contains("apk")){
                        Log.d("sss", "不是apk文件啊=="+apk.getName());
                       continue;
                    }
                    try {
                        tvTest.setText(apk.getAbsolutePath());
                        Log.d("sss", "即将安装的apk=="+apk.getAbsolutePath());
                        //a = PluginManager.getInstance().installPackage(plugins[0].getAbsolutePath(), 0);//安装第一个插件
                        a =PluginManager.getInstance().installPackage(apk.getAbsolutePath(), PackageManagerCompat.INSTALL_REPLACE_EXISTING);

                        getResult(a);
                    } catch (RemoteException e) {
                        Log.d("sss", "插件安装失败=="+e.getMessage().toString());
                        e.printStackTrace();

                    }
                }


            }
        });

        //启动APK插件
        btnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PackageManager pm = getPackageManager();

                    Intent intent = pm.getLaunchIntentForPackage("com.mhook.dialog");

                    if (intent == null){
                        Log.d("sss","intent是空的，没法使用啊");
                    }
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("sss", "插件启动失败==" + e.toString());
                }
            }
        });
    }

    private void getResult(int a) {
        switch (a) {
            case -1:
                tvResult.setText("安装或卸载失败");
                break;
            case 1:
                tvResult.setText("安装或卸载成功");
                break;
            case -110:
                tvResult.setText("安装程序内部错误");
                break;
            case -2:
                tvResult.setText("无效的Apk");
                break;
            case 0x00000002:
                tvResult.setText("安装更新");
                break;
            case -3:
                tvResult.setText("不支持的ABI");
                break;

            default:
                tvResult.setText("老天都不知道这是咋了,a=="+a);
                break;
        }

    }

}
